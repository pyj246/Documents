### OS : CentOS 7.1_1503 64bit

#### 1. 기본 패키지 설치

```
[root]# yum check-update
[root]# yum -y upgrade
```
    
```
[root]# yum install setup
[root]# yum install setuptool
[root]# yum install system-config-securitylevel-tui
[root]# yum install net-tools
[root]# yum install vsftpd
[root]# yum install vsftpd-sysvinit
[root]# yum install wget
[root]# yum install rdate
```
Epel Repository 설치
```
[root]# yum install epel-release
```
SELINUX 비활성화
```
[root]# setenforce 0
[root]# vi /etc/sysconfig/selinux
...
SELINUX=disabled
...
```


  
#### 2. OpenLDAP 설치
- OpenLDAP version : 2.4.0

OpenLDAP 패키지 설치  
```
[root]# yum -y install openldap openldap-clients openldap-servers
```
DB_CONFIG 파일 복사
```
[root]# cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG 
```
패스워드 설정
- 패스워드 입력후 나온 {SSHA}키를 따로 복사해 놓는다.

```
[root]# slappasswd 
New password : 패스워드입력
Re-enter new password : 패스워드입력
{SSHA}~~~~~~
```
olcDatabase\=\{1\}monitor.ldif 편집
- dc값을 변경한다.

```
[root]# vi /etc/openldap/slapd.d/cn\=config/olcDatabase\=\{1\}monitor.ldif
...
olcAccess: {0}to * by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" read by dn.base="cn=Manager,dc=crscube,dc=co,dc=kr" read by * none
...
```
olcDatabase\=\{2\}hdb.ldif 편집
- "olcSuffix"과 "olcRootDN" 항목의 dc값을 변경한다.
- "olcRootPW" 에는 패스워드 설정을 하고 나온 결과값을 복사하여 넣어준다. 만약 항목이 없다면 추가한다.

```
[root]# vi /etc/openldap/slapd.d/cn\=config/olcDatabase\=\{2\}hdb.ldif
...
olcSuffix: dc=crscube,dc=co,dc=kr
olcRootDN: cn=Manager,dc=crscube,dc=co,dc=kr
olcRootPW: openLDAP 패스워드의 {SSHA}키
...
[root]# systemctl start slapd
[root]# systemctl stop slapd
```
slapd.conf 파일 생성
```
[root]# vi /etc/openldap/slapd.conf
include         /etc/openldap/schema/corba.schema
include         /etc/openldap/schema/core.schema
include         /etc/openldap/schema/cosine.schema
include         /etc/openldap/schema/duaconf.schema
include         /etc/openldap/schema/dyngroup.schema
include         /etc/openldap/schema/inetorgperson.schema
include         /etc/openldap/schema/java.schema
include         /etc/openldap/schema/misc.schema
include         /etc/openldap/schema/nis.schema
include         /etc/openldap/schema/openldap.schema
include         /etc/openldap/schema/ppolicy.schema
include         /etc/openldap/schema/collective.schema

allow bind_v2

pidfile         /var/run/openldap/slapd.pid
argsfile        /var/run/openldap/slapd.args

TLSCACertificatePath /etc/openldap/certs
TLSCertificateFile "\"OpenLDAP Server\""
TLSCertificateKeyFile /etc/openldap/certs/password

database config
access to *
        by dn.exact="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" manage
        by * none

database monitor
access to *
        by dn.exact="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" read 
        by dn.exact="cn=Manager,dc=crscube,dc=co,dc=kr" read
        by * none

database        hdb
suffix          "dc=crscube,dc=co,dc=kr"
checkpoint      1024 15
rootdn          "cn=Manager,dc=crscube,dc=co,dc=kr"
rootpw          openLDAP 패스워드의 {SSHA}키

directory       /var/lib/ldap

index objectClass                       eq,pres
index ou,cn,mail,surname,givenname      eq,pres,sub
index uidNumber,gidNumber,loginShell    eq,pres
index uid,memberUid                     eq,pres,sub
index nisMapName,nisMapEntry            eq,pres,sub

access to attrs=userPassword
        by self write
        by * none
[root]# slaptest -f /etc/openldap/slapd.conf -F /etc/openldap/slapd.d 
[root]# rm /etc/openldap/slapd.d/cn\=config/cn\=schema/cn\={0\}core.ldif
[root]# chown ldap:ldap -R /etc/openldap/slapd.d/cn\=config/cn\=schema
```
OpenLDAP 시작 및 자동시작 등록
```
[root]# systemctl enable slapd
[root]# systemctl start slapd
```
OpenLDAP 기본 Directory 파일 생성 
```
[root]# cd /tmp
[root]# vi crscube.ldif
dn: dc=crscube,dc=co,dc=kr
objectClass: domain
dc: crscube
o : crscube
[root]# ldapadd -f crscube.ldif -D cn=Manager,dc=crscube,dc=co,dc=kr -W
Enter LDAP Password: 입력한 패스워드
[root]# ldapsearch -x -LLL -b dc=crscube,dc=co,dc=kr
dn: dc=crscube,dc=co,dc=kr
objectClass: domain
dc: crscube
o: crscube
```
방화벽 등록
- (port: tcp 389)

```
[root]# firewall-cmd --permanent --add-service=ldap
[root]# firewall-cmd --reload
```
시스템 로그 등록
- "rsyslog.conf"파일에 "local4.* /var/log/ldap.log" 추가

```
[root]# vi /etc/rsyslog.conf
...
local4.* /var/log/ldap.log
[root]# systemctl restart rsyslog
```

#### 3. NGINX 설치
- NGINX version : 1.6.3

방화벽 오픈
```
[root]# firewall-cmd --permanent --add-service=http
[root]# firewall-cmd --permanent --add-service=https
[root]# systemctl reload firewalld
```
NGINX 설치 후 기동
```
[root]# yum -y install nginx
[root]# systemctl start nginx
```
설치 후 웹브라우저에서 http://"설치서버 HOST" 를 호출하여 NGINX 기본 페이지가 뜨는 것을 확인한다.

#### 4. PHP 설치 및 NGINX와 연동
 - PHP version : 5.4.16

PHP 설치 후 NGINX와 연동
```
[root]# yum -y install php php-mbstring php-pear php-fpm php-ldap
[root]# vi /etc/php-fpm.d/www.conf
...
listen = /var/run/php-fpm/php-fpm.sock
...
user = nginx
...
group = nginx
...
[root]# systemctl enable php-fpm
[root]# systemctl start php-fpm
```


#### 5. PHPLDAPAdmin 설치
- PHPLDAPAdmin version : 1.2.3

PHPLDAPAdmin 설치
```
[root]# yum --enablerepo=epel -y install phpldapadmin
```
PHPLDAPAdmin 설정
```
[root]# chown nginx:nginx /etc/phpldapadmin/config.php
[root]# chown nginx:nginx /var/lib/php/session/
[root]# vi /etc/phpldapadmin/config.php
...
*/

$servers->setValue('server', 'base', array('dc=crscube,dc=co,dc=kr'));
$servers->setValue('login', 'bind_id', 'cn=Manager,dc=crscube,dc=co,dc=kr');
$servers->setValue('login', 'bind_pass', 'openLDAP 패스워드의 {SSHA}키');
$servers->setValue('login', 'attr', 'dn');

?>
```
NGINX와 연동
```
[root]# vi /etc/nginx/conf.d/phpldapadmin.conf
server {
	listen 80;
    server_name LDAP도메인;
    root /usr/share/phpldapadmin/htdocs/;
    index index.php
    
    include /etc/nginx/default.d/*.conf;
    
    location ~ \.php$ {
    	fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
[root]# nginx -t
[root]# systemctl restart nginx
```

#### 6. GitLab 설치
- GitLab version : 7.14.1

openSSH 및 메일 서버 설치
```
[root]# yum install curl openssh-server
[root]# systemctl enable sshd
[root]# systemctl start sshd
[root]# yum install postfix
[root]# systemctl enable postfix
[root]# systemctl start postfix
```
GitLab 설치용 bash 파일 만들기
```
[root]# cd /tmp
[root]# vi bash
```
bash 파일
```
#!/bin/bash

major_version=
os=
host=

get_hostname ()
{
  echo "Getting the hostname of this machine..."

  host=`hostname -f 2>/dev/null`
  if [ "$host" = "" ]; then
    host=`hostname 2>/dev/null`
    if [ "$host" = "" ]; then
      host=$HOSTNAME
    fi
  fi

  if [ "$host" = "" ]; then
    echo "Unable to determine the hostname of your system!"
    echo
    echo "Please consult the documentation for your system. The files you need "
    echo "to modify to do this vary between Linux distribution and version."
    echo
    exit 1
  fi

  echo "Found hostname: ${host}"
}

curl_check ()
{
  echo "Checking for curl..."
  if command -v curl > /dev/null; then
    echo "Detected curl..."
  else
    echo "Installing curl..."
    yum install -d0 -e0 -y curl
  fi
}

unknown_os ()
{
  echo "Unfortunately, your operating system distribution and version are not supported by this script."
  echo "Please email support@packagecloud.io and we will be happy to help."
  exit 1
}

if [ -e /etc/os-release ]; then
  . /etc/os-release
  major_version=`echo ${VERSION_ID} | awk -F '.' '{ print $1 }'`
  os=${ID}

elif [ `which lsb_release 2>/dev/null` ]; then
  # get major version (e.g. '5' or '6')
  major_version=`lsb_release -r | cut -f2 | awk -F '.' '{ print $1 }'`

  # get os (e.g. 'centos', 'redhatenterpriseserver', etc)
  os=`lsb_release -i | cut -f2 | awk '{ print tolower($1) }'`

elif [ -e /etc/oracle-release ]; then
  major_version=`cut -f5 --delimiter=' ' /etc/oracle-release | awk -F '.' '{ print $1 }'`
  os='ol'

elif [ -e /etc/fedora-release ]; then
  major_version=`cut -f3 --delimiter=' ' /etc/fedora-release`
  os='fedora'

elif [ -e /etc/redhat-release ]; then
  os_hint=`cat /etc/redhat-release  | awk '{ print tolower($1) }'`
  if [ "${os_hint}" = "centos" ]; then
    major_version=`cat /etc/redhat-release | awk '{ print $3 }' | awk -F '.' '{ print $1 }'`
    os='centos'
  elif [ "${os_hint}" = "scientific" ]; then
    major_version=`cat /etc/redhat-release | awk '{ print $4 }' | awk -F '.' '{ print $1 }'`
    os='scientific'
  else
    major_version=`cat /etc/redhat-release  | awk '{ print tolower($7) }' | cut -f1 --delimiter='.'`
    os='redhatenterpriseserver'
  fi

else
  aws=`grep -q Amazon /etc/issue`
  if [ "$?" = "0" ]; then
    major_version='6'
    os='aws'
  else
    unknown_os
  fi
fi

if [[ ( -z "${os}" ) || ( -z "${major_version}" ) || ( "${os}" = "opensuse" ) ]]; then
  unknown_os
fi

echo "Detected ${os} version ${major_version}... "

curl_check

get_hostname

yum_repo_path=/etc/yum.repos.d/gitlab_gitlab-ce.repo
yum_repo_config_url="https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/config_file.repo?os=${os}&dist=${major_version}&name=${host}&source=script"

echo "Downloading repository file: ${yum_repo_config_url}"

curl -f "${yum_repo_config_url}" > $yum_repo_path
curl_exit_code=$?

if [ "$curl_exit_code" = "22" ]; then
  echo
  echo -n "Unable to download repo config from: "
  echo "${yum_repo_config_url}"
  echo
  echo "Please contact support@packagecloud.io and report this."
  [ -e $yum_repo_path ] && rm $yum_repo_path
  exit 1
elif [ "$curl_exit_code" = "35" ]; then
  echo
  echo "curl is unable to connect to packagecloud.io over TLS when running: "
  echo "    curl ${yum_repo_config_url}"
  echo
  echo "This is usually due to one of two things:"
  echo
  echo " 1.) Missing CA root certificates (make sure the ca-certificates package is installed)"
  echo " 2.) An old version of libssl. Try upgrading libssl on your system to a more recent version"
  echo
  echo "Contact support@packagecloud.io with information about your system for help."
  [ -e $yum_repo_path ] && rm $yum_repo_path
  exit 1
elif [ "$curl_exit_code" -gt "0" ]; then
  echo
  echo "Unable to run: "
  echo "    curl ${yum_repo_config_url}"
  echo
  echo "Double check your curl installation and try again."
  [ -e $yum_repo_path ] && rm $yum_repo_path
  exit 1
else
  echo "done."
fi

echo "Installing pygpgme to verify GPG signatures..."
yum install -y pygpgme --disablerepo='gitlab_gitlab-ce'
pypgpme_check=`rpm -qa | grep -qw pygpgme`
if [ "$?" != "0" ]; then
  echo
  echo "WARNING: "
  echo "The pygpgme package could not be installed. This means GPG verification is not possible for any RPM installed on your system. "
  echo "To fix this, add a repository with pygpgme. Usualy, the EPEL repository for your system will have this. "
  echo "More information: https://fedoraproject.org/wiki/EPEL#How_can_I_use_these_extra_packages.3F"
  echo

  # set the repo_gpgcheck option to 0
  sed -i'' 's/repo_gpgcheck=1/repo_gpgcheck=0/' /etc/yum.repos.d/gitlab_gitlab-ce.repo
fi

echo "Installing yum-utils..."
yum install -y yum-utils --disablerepo='gitlab_gitlab-ce'
yum_utils_check=`rpm -qa | grep -qw yum-utils`
if [ "$?" != "0" ]; then
  echo
  echo "WARNING: "
  echo "The yum-utils package could not be installed. This means you may not be able to install source RPMs or use other yum features."
  echo
fi

echo "Generating yum cache for gitlab_gitlab-ce..."
yum -q makecache -y --disablerepo='*' --enablerepo='gitlab_gitlab-ce'
```
GitLab 설치
```
[root]# curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash
[root]# yum -y install gitlab-ce
```
GitLab Reconfigure 실행
```
[root]# gitlab-ctl reconfigure
```
NGINX와 연동
- GitLab에 Embeded된 NGINX가 있으나 위에서 설치한 NGINX에 통합하여 사용한다.
- GitLab의 포트를 8080에서 9010으로 변경한다.

```
[root]# gitlab-ctl stop
[root]# cp /var/opt/gitlab/nginx/conf/gitlab* /etc/nginx/conf.d/
[root]# vi /etc/nginx/conf.d/gitlab-http.conf
...
server_name GitLab도메인
...
access_log /var/log/nginx/gitlab_access.log
error_log /var/log/nginx/gitlab_error.log
...
[root]# vi /etc/gitlab/gitlab.rb
...
web_server['external_users'] = ['nginx']
nginx['enable'] = false
unicorn['port'] = 9010
...
[root]# gitlab-ctl reconfigure
[root]# gitlab-ctl start
[root]# nginx -t
[root]# systemctl restart nginx
```
OpenLDAP 연동
```
[root]# vi /etc/gitlab/gitlab.rb
...
gitlab_rails['ldap_enabled'] = true
gitlab_rails['ldap_servers'] = YAML.load <<-'EOS' # remember to close this block with 'EOS' below
  main: # 'main' is the GitLab 'provider ID' of this LDAP server
    label: 'LDAP'
    host: 'localhost'
    port: 389
    uid: 'uid'
    method: 'plain' # "tls" or "ssl" or "plain"
    bind_dn: 'cn=Manager,dc=crscube,dc=co,dc=kr'
    password: 'openLDAP 패스워드의 {SSHA}키'
    active_directory: true
    allow_username_or_email_login: false
    block_auto_created_users: false
    base: 'dc=crscube,dc=co,dc=kr'
    user_filter: ''
    ## EE only
    group_base: ''
    admin_group: ''
    sync_ssh_keys: false
EOS
...
[root]# gitlab-ctl reconfigure
[root]# gitlab-ctl restart
```
브라우저에서 http://GitLab도메인 을 통해 접속하여 정상적으로 동작하는지 확인한다.
정상적으로 동작한다면 아래의 계정으로 접속하여 Password를 변경한다.
패스워드 변경 후 [Admin Area] - [Settings] - [Sign-in Restrictions] - [Sign-up enabled] 를 비활성화 하고 [Save] 한다.
Username: root 
Password: 5iveL!fe

#### 7. Jenkins 설치
- Jenkins version : 1.628
- Java version : 1.8.0_51-b16

자바 설치
```
[root]# yum -y install java
```
Jenkins 설치
```
[root]# wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo
[root]# rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key
[root]# yum -y install jenkins
[root]# vi /etc/sysconfig/jenkins
...
JENKINS_PORT="9020"
...
[root]# systemctl enable jenkins
[root]# systemctl start jenkins
```
NGINX와 연동
```
[root]# vi /etc/nginx/conf.d/jenkins.conf
server {
  listen          80;
  server_name     Jenkins도메인;

  root            /var/run/jenkins/war/;

  access_log      /var/log/nginx/jenkins_access.log;
  error_log       /var/log/nginx/jenkins_error.log;

  location ~ "^/static/[0-9a-fA-F]{8}\/(.*)$" {
    rewrite "^/static/[0-9a-fA-F]{8}\/(.*)" /$1 last;
  }

  location /userContent {
	root /var/lib/jenkins/;
        if (!-f $request_filename){
           #this file does not exist, might be a directory or a /**view** url
           rewrite (.*) /$1 last;
	   break;
        }
	sendfile on;
  }

  location @jenkins {
      sendfile off;
      proxy_pass         http://127.0.0.1:9020;
      proxy_redirect     default;

      proxy_set_header   Host             $host;
      proxy_set_header   X-Real-IP        $remote_addr;
      proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
      proxy_max_temp_file_size 0;

      client_max_body_size       10m;
      client_body_buffer_size    128k;

      proxy_connect_timeout      90;
      proxy_send_timeout         90;
      proxy_read_timeout         90;

      proxy_buffer_size          4k;
      proxy_buffers              4 32k;
      proxy_busy_buffers_size    64k;
      proxy_temp_file_write_size 64k;
  }

  location / {
      if ($http_user_agent ~* '(iPhone|iPod)') {
          rewrite ^/$ /view/iphone/ redirect;
      }
      try_files $uri @jenkins;
   }
}
[root]# systemctl restart nginx
```