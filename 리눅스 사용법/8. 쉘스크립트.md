### 쉘 스크립트
- Redhat 계열의 bash를 기준으로 한다.

1. 변수
- 모든 변수는 문자열(string)으로 취급된다.
- 변수 이름은 대소문자를 구분한다.
- 변수를 대입(선언)할때 '='의 좌우에는 공백이 없어야한다.
```
testval1=Hello
# testval1 = Hello 는 오류이다.
testval2=1
testval3="Hello World"
# testval3=Hello World 는 오류이다.
```
- 변수를 사용할때는 ****$**** 를 변수명 앞에 붙여서 사용한다.
```
#!/bin/sh
myvar="Hi Variable"
echo $myvar
# → Hi Variable
echo "$myvar"
# → Hi Variable
echo '$myvar'
# → $myvar
echo \$myvar
# → $myvar
exit 0
```
- 숫자계산
	****\`expr 계산식\`****을 사용한다.
    ****\`****은 역따옴표이다. (키보드 숫자1 왼쪽의 문자)
```
#!/bin/sh
num1=100
num2=$num1+200
echo $num2
# → 100+200
num3=`expr $num1 + 200`
echo $num3
# → 300
num4=`expr \( $num1 + 200 \) /10 \* 2`
echo $num4
# → 60
exit 0
```
- 명령 인자 (argument)
	$0, $1, $2
```
$ sh /tmp/test.sh 값1 값2 값3
# $0 → /tmp/test.sh
# $1 → 값1
# $2 → 값2
# $3 → 값3
# $* → 값1 값2 값3($0을 제외한 파라미터이다.)
```
```
#!/bin/bash
echo "This Script Executable File : $0"
echo "Argument Count : $#"
echo "Argument List \$* : $*"
echo "Argument List \$@ : $@"
echo "Argument 1 : $1"
echo "Argument 2 : $2"
echo "Argument 3 : $3"
echo "Argument 4 : $4"
```

2.분기문, 조건
- 조건
	* 문자열
	```
    # "문자열1" = "문자열2" → 두 문자열이 같으면 참
    # "문자열1" != "문자열2" → 두 문자열이 같지 않으면 참
    # -n "문자열1" → 문자열이 null이 아니면 참
    # -z "문자열1" → 문자열이 null이면 참
    ```
	* 산술비교
	```
    # 수식1 -eq 수식2 → 두 수식(또는 변수)이 같으면 참
    # 수식1 -ne 수식2 → 두 수식(또는 변수)이 같으면 참
    # 수식1 -gt 수식2 → 수식1이 크다면 참
    # 수식1 -ge 수식2 → 수식1이 그거나 같으면 참
    # 수식1 -lt 수식2 → 수식1이 작으면 참
    # 수식1 -le 수식2 → 수식1이 작거나 같으면 참
    # !수식 → 수식이 거짓이면 참
    ```
	* 파일관련
	```
    # -e 파일이름 → 파일이 존재하면 참
    # -s 파일이름 → 파일 크기가 0이 아니면 참
    # -f 파일이름 → 파일이 일반파일이면 참
    # -d 파일이름 → 파일이 디렉토리이면 참
    # -u 파일이름 → 파일이 set-user-id가 설정되면 참
    # -g 파일이름 → 파일이 set-group-id가 설정되면 참
    # -r 파일이름 → 파일이 읽기 가능이면 참
    # -w 파일이름 → 파일이 쓰기 가능이면 참
    # -x 파일이름 → 파일이 실행 가능이면 참
    ```
    * AND, OR 관계연산자
    	AND : ****-a**** 또는 ****&&****
        OR : ****-o**** 또는 ****||****
- if~fi문
	if [조건]
    then 
    	'참일경우 실행'
    fi
```
#!/bin/sh
if ["woo" = "woo"]
then
	echo "참입니다."
fi
exit 0
```
- if~else~fi문
	if [조건1]
    then 
    	조건1이 참일 경우 실행
    elif [조건2]
    then
    	조건2가 참일 경우 실행
    else 
   		조건1, 조건2가 참이 아닐 경우 실행
    fi
```
#!/bin/sh
testfile=/tmp/test.txt
if [ -d $testfile ]
then 
	echo "디렉토리입니다."
elif [ -f $tstfile ]    
then
	echo "일반 파일이니다."
else
	echo "디렉토리나 일반 파일이 아닙니다."
fi
```
- case~esac문
```
#!/bin/sh
case "$1" in
    start)
        echo "시작";;
    stop)
        echo "정지";;
    restart)
        echo "다시 시작";;
    *)
    	echo "???";;
esac
exit 0
```

3.반복문
- 반복문 제어
	break : 반복문을 중단한다.
    continue : 현재 수행중인 반복을 중단하고 다음 반복을 수행한다.
- for문
	for 변수 in 값1 값2 값3
    do
    반복 수행할 작업
    done
```
#!/bin/sh
if [ `expr $#` -gt 0 ]
then
        for arg in $@
        do
                echo $arg
        done
else
        echo "Not exists arguments"
fi
exit 0
```
- while
	조건이 참인 동안에 계속 반복한다.
    조건식에 [1]이나 [:]가 오면 항상 참이 된다. (무한루프)
    while [조건]
    do
	반복 수행할 작업
    done
```
#!/bin/sh
hap=0
i=1
while [ $i -le 10 ]
do
        hap=`expr $hap + $i`
        i=`expr $i + 1`
done
echo "1부터 10까지의 합: "$hap
exit 0
```
- until문
	조건이 참일 때까지(거짓인 동안) 계속 반복한다.
 	until [조건]
    do
    반복 수행할 작업
    done
```
#!/bin/sh
hap=0
i=1
until [ $i -gt 10 ]
do
        hap=`expr $hap + $i`
        i=`expr $i + 1`
done
echo "1부터 10까지의 합: "$hap
exit 0
```

4.함수
- 사용자가 직접 함수를 작성하고 호출한다.
    함수이름 () {
    수행할 작업
    }
```
#!/bin/sh
myFunction() {
	echo "함수 안으로 들어왔음"
    return
}
echo "프로그램을 시작합니다."
myFunction
echo "프로그램을 종료합니다."
exit 0
```
```
#!/bin/sh
hap () {
	echo 'expr $1 + $2'
}
echo "10 더하기 20을 실행합니다.
hap 10 20
exit 0
```

5.기타
- eval
	문자열을 명령문으로 인식하고 실행한다.
```
#!/bin/bash
str="ls -l /tmp.test.txt"
echo $str
eval $str
exit 0
```
- export
	외부 변수로 선언한다.
```
#!/binsh
var1="지역변수"
export var2="외부변수"
exit 0
```
- set과 $(명령어)
	리눅스의 명령어를 결과로 사용하기 위해서는 ****$(명령어)**** 형식으로 사용한다.
    결과를 파라미터로 사용하고자 할때는 ****set****을 사용한다.
```
#!/bin/bash
echo "오늘 날짜는 $(date) 입니다."
set $(date)
echo "오늘은 $4 요일입니다."
exit 0
```
- shift
	10개가 넘는 파라미터(명령 인자)에 접근할때 사용한다.
```
#!/bin/sh
myfunc() {
	echo $1 $2 $3 $4 $5 $6 $7 $8 $9 $10 $11
}
myfunction AAA BBB CCC DDD EEE FFF GGG HHH III JJJ KKK
exit 0
# AAA BBB CCC DDD EEE FFF GGG HHH III AAA0 AAA1
```
```
#!/bin/sh
myfunc() {
	str=""
    while [ "$1" != "" ];
    do
    	str="str $1"
        shife
    done
	echo $str
}
myfunction AAA BBB CCC DDD EEE FFF GGG HHH III JJJ KKK
exit 0
# AAA BBB CCC DDD EEE FFF GGG HHH III JJJ KKK
```